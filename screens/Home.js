import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView,
    BackHandler
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
//import { LinearGradient } from "expo";
import { LinearGradient } from 'expo-linear-gradient'
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import Card from "./Home/Card";
import Promotion from "./Home/Promotion";
import BottomBar from "./Shared/BottomBar";
import LoadingOverlay from "./Shared/LoadingOverlay";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 1,
            tutorialStep: 0,
            fromScan: false,
            ready: false,
            promoTab: 0
        };
    }

    componentDidMount() {
        this.setState({
            fromScan: this.props.navigation.getParam("fromScan", false)
        });
        if (this.props.persona.churnPic.uri == "") {
            this.setState({
                tutorialStep: 1
            });
        }
        //this._fetchCards();
    }

    _fetchCards = async () => {
        await fetch(
            `${Expo.Constants.manifest.extra.fxUrl}CardsByCust?code=${
                Expo.Constants.manifest.extra.fxKey
            }&customerId=${this.props.persona.custId}`,
            {}
        )
            .then(res => res.json())
            .then(res => {
                let persona = { ...this.props.persona };

                let cards = res.reduce((total, value) => {
                    return total.concat({
                        png: { uri: value.card_pic },
                        name: value.name_on_card,
                        number: value.card_number,
                        cardName: value.card_type,
                        balance: value.availabel_point,
                        cardId: value.card_id
                    });
                }, []);

                persona.cards = cards;
                this.props.updatePersona(persona);
                this._fetchPromotions();
            })
            .catch(error => {
                console.log(error.message);
                Alert.alert(
                    "Connection Problem",
                    "Please connect internet and press retry",
                    [
                        {
                            text: "Retry",
                            onPress: () => {
                                this._fetchCards();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            });
    };

    _fetchPromotions = async () => {
        await fetch(
            `${Expo.Constants.manifest.extra.fxUrl}PromosByCust?code=${
                Expo.Constants.manifest.extra.fxKey
            }&customerId=${this.props.persona.custId}`,
            {}
        )
            .then(res => res.json())
            .then(res => {
                let persona = { ...this.props.persona };

                let promos = res.promos.reduce((total, value) => {
                    return total.concat({
                        png: {
                            uri: value.campaign_pic
                        },
                        promoId: value.campaign_id,
                        promoName: value.campaign_name,
                        isPersonalized: value.is_personalized,
                        itemCode: value.item_code,
                        itemName: value.item_name,
                        itemDesc: value.item_desc,
                        promoPrice: value.campaign_price
                    });
                }, []);

                let specialPromos = res.specialPromos.reduce((total, value) => {
                    return total.concat({
                        png: {
                            uri: value.campaign_pic
                        },
                        promoId: value.campaign_id,
                        promoName: value.campaign_name,
                        isPersonalized: value.is_personalized,
                        itemCode: value.item_code,
                        itemName: value.item_name,
                        itemDesc: value.item_desc,
                        promoPrice: value.campaign_price
                    });
                }, []);

                persona.promos = promos;
                persona.specialPromos = specialPromos;
                persona.allPromos = specialPromos.concat(promos);
                this.props.updatePersona(persona);
                this.setState({ ready: true });
            })
            .catch(error => {
                console.log(error.message);
                Alert.alert(
                    "Connection Problem",
                    "Please connect internet and press retry",
                    [
                        {
                            text: "Retry",
                            onPress: () => {
                                this._fetchPromotions();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            });
    };

    _renderCard = ({ item, index }) => {
        return <Card item={item} />;
    };

    _renderPromotion = ({ item, index }) => {
        return <Promotion item={item} />;
    };

    render() {
        const concatPromos = this.props.persona.promos.concat(
            this.props.persona.specialPromos
        );
        return (
            <LinearGradient
                style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "space-between"
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <ScrollView
                    style={{
                        zIndex: 1,
                        height: SCREEN_HEIGHT - 88
                    }}
                >
                    <View
                        style={{
                            zIndex: 1
                        }}
                    >
                        <View style={{ height: 40 }} />
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 22,
                                fontFamily: "Prompt",
                                color: "#ffffff"
                            }}
                        >
                            Welcome! {this.props.persona.name}
                        </Text>
                        <View style={{ height: 15 }} />
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 18,
                                fontFamily: "Prompt",
                                color: "#ffffff"
                            }}
                        >
                            {this.props.persona.describe}
                        </Text>
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 14,
                                fontFamily: "Prompt_LightItalic",
                                color: "#ffffff"
                            }}
                        >
                            {this.props.persona.subDescribe}
                        </Text>

                        <View style={{ height: 15 }} />
                        <View
                            style={{
                                width: SCREEN_WIDTH,
                                height: ((SCREEN_WIDTH - 80) * 322) / 534
                            }}
                        >
                            <Carousel
                                data={this.props.persona.cards}
                                renderItem={this._renderCard}
                                sliderWidth={SCREEN_WIDTH}
                                itemWidth={SCREEN_WIDTH - 80}
                                onSnapToItem={index => {
                                    this.setState({
                                        activeSlide: index
                                    });
                                }}
                                firstItem={this.state.activeSlide}
                            />
                        </View>
                        <Pagination
                            dotsLength={this.props.persona.cards.length}
                            activeDotIndex={this.state.activeSlide}
                            containerStyle={{
                                marginTop: -10,
                                marginBottom: -10
                            }}
                            dotStyle={{
                                width: 10,
                                height: 10,
                                borderRadius: 5,
                                marginHorizontal: -10,
                                backgroundColor: "rgba(255, 255, 255, 0.92)"
                            }}
                            inactiveDotStyle={
                                {
                                    // Define styles for inactive dots here
                                }
                            }
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                        />
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "space-between"
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    flex: 1
                                }}
                                onPress={() => {
                                    this.setState({
                                        promoTab: 0
                                    });
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 18,
                                        fontFamily: "Prompt",
                                        color: "#ffffff",
                                        textAlign: "center"
                                    }}
                                >
                                    S1 For You
                                </Text>

                                {this.state.promoTab == 0 && (
                                    <LinearGradient
                                        style={{
                                            width: 120,
                                            height: 4,
                                            alignSelf: "center"
                                        }}
                                        colors={["#c19963", "#f7ce68"]}
                                        start={[1, 0.5]}
                                        end={[0, 0.5]}
                                    />
                                )}
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={{
                                    flex: 1
                                }}
                                onPress={() => {
                                    this.setState({
                                        promoTab: 1
                                    });
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 18,
                                        fontFamily: "Prompt",
                                        color: "#ffffff",
                                        textAlign: "center"
                                    }}
                                >
                                    Promotions
                                </Text>
                                {this.state.promoTab == 1 && (
                                    <LinearGradient
                                        style={{
                                            width: 120,
                                            height: 4,
                                            alignSelf: "center"
                                        }}
                                        colors={["#c19963", "#f7ce68"]}
                                        start={[1, 0.5]}
                                        end={[0, 0.5]}
                                    />
                                )}
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 20 }} />
                        {this.state.promoTab == 0 && (
                            <Carousel
                                data={this.props.persona.specialPromos}
                                renderItem={this._renderPromotion}
                                sliderWidth={SCREEN_WIDTH}
                                itemWidth={SCREEN_WIDTH - 170}
                                // firstItem={1}
                            />
                        )}
                        {this.state.promoTab == 1 && (
                            <Carousel
                                data={this.props.persona.promos}
                                renderItem={this._renderPromotion}
                                sliderWidth={SCREEN_WIDTH}
                                itemWidth={SCREEN_WIDTH - 170}
                                // firstItem={1}
                            />
                        )}
                    </View>
                </ScrollView>
                <BottomBar />
                <Image
                    style={{
                        position: "absolute",
                        left: 0,
                        right: 0,
                        bottom: 0
                    }}
                    width={SCREEN_WIDTH}
                    height={(SCREEN_WIDTH * 400) / 750}
                    source={require("../assets/Element/waveLeft.png")}
                />
                {this.state.fromScan && (
                    <View
                        style={[
                            StyleSheet.absoluteFillObject,
                            {
                                backgroundColor: "rgba(2, 2, 2, 0.8)",
                                zIndex: 2,
                                alignItems: "center"
                            }
                        ]}
                    >
                        <View style={{ height: 40 }} />
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 22,
                                fontFamily: "Prompt",
                                color: "#ffffff"
                            }}
                        >
                            Welcome! {this.props.persona.name}
                        </Text>
                        <View style={{ height: 15 }} />
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 18,
                                fontFamily: "Prompt",
                                color: "#ffffff"
                            }}
                        >
                            {this.props.persona.describe}
                        </Text>
                        <Text
                            style={{
                                alignSelf: "center",
                                fontSize: 14,
                                fontFamily: "Prompt_LightItalic",
                                color: "#ffffff"
                            }}
                        >
                            {this.props.persona.subDescribe}
                        </Text>
                        {this.state.tutorialStep <= 1 && (
                            <React.Fragment>
                                <View style={{ height: 10 }} />
                                <ScrollView>
                                    <ImageBackground
                                        style={
                                            this.state.tutorialStep == 0
                                                ? {
                                                      alignSelf: "center",
                                                      width: SCREEN_WIDTH - 40,
                                                      height: SCREEN_WIDTH - 40
                                                  }
                                                : {
                                                      alignSelf: "center",
                                                      width: SCREEN_WIDTH - 40,
                                                      height:
                                                          ((SCREEN_WIDTH - 40) *
                                                              1321) /
                                                          352
                                                  }
                                        }
                                        source={
                                            this.state.tutorialStep == 0
                                                ? this.props.persona.churnPic
                                                : this.props.persona.promoPic
                                        }
                                    >
                                        <TouchableOpacity
                                            style={{
                                                position: "absolute",
                                                top: 0,
                                                right: 0,
                                                marginTop: 16,
                                                marginRight: 16
                                            }}
                                            onPress={() => {
                                                this.setState({
                                                    tutorialStep:
                                                        this.state
                                                            .tutorialStep + 1
                                                });
                                            }}
                                        >
                                            <Image
                                                style={{
                                                    width: 30,
                                                    height: 30
                                                }}
                                                source={require("../assets/Element/close.png")}
                                            />
                                        </TouchableOpacity>
                                    </ImageBackground>
                                    <View style={{ height: 10 }} />
                                </ScrollView>
                            </React.Fragment>
                        )}
                        {this.state.tutorialStep == 2 && (
                            <React.Fragment>
                                <View
                                    style={{
                                        height: 15
                                    }}
                                />
                                <Card
                                    item={
                                        this.props.persona.cards[
                                            this.state.activeSlide
                                        ]
                                    }
                                />
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        letterSpacing: 0.2,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}
                                >
                                    บัตรและยอดเงินคงเหลือของคุณ
                                </Text>
                                <View
                                    style={{
                                        position: "absolute",
                                        bottom: 88,
                                        left: 0,
                                        right: 0,
                                        height: 110,
                                        width: SCREEN_WIDTH,
                                        alignItems: "center"
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            fontFamily: "Prompt",
                                            color: "#fff"
                                        }}
                                    >
                                        เริ่ม ScanQR ที่สินค้าได้เลย!
                                    </Text>
                                    <SvgUri
                                        width={62}
                                        height={82}
                                        source={require("../assets/MainMenu/down.svg")}
                                    />
                                </View>
                                <View
                                    style={{
                                        position: "absolute",
                                        bottom: 0,
                                        left: 0,
                                        right: 0,
                                        height: 88,
                                        width: SCREEN_WIDTH,
                                        justifyContent: "center"
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                fromScan: false
                                            });
                                            this.props.navigation.navigate(
                                                "ScanProduct"
                                            );
                                        }}
                                        style={{
                                            alignSelf: "center",
                                            alignItems: "center"
                                        }}
                                    >
                                        <SvgUri
                                            width={40}
                                            height={40}
                                            source={require("../assets/MainMenu/qrcode.svg")}
                                        />
                                        <Text
                                            style={{
                                                fontSize: 16,
                                                fontFamily: "Prompt",
                                                color: "#ffffff",
                                                textAlign: "center"
                                            }}
                                        >
                                            Scan QR
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </React.Fragment>
                        )}
                    </View>
                )}

                {!this.state.ready && <LoadingOverlay />}
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
