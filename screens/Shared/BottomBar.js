import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
//import { LinearGradient } from "expo";
import { LinearGradient } from 'expo-linear-gradient'
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { withNavigation } from "react-navigation";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class BottomBar extends React.PureComponent {
    render() {
        return (
            <LinearGradient
                style={{
                    flexDirection: "row",
                    alignItems: "center",
                    height: 88,
                    width: SCREEN_WIDTH,
                    zIndex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[0, 0.5]}
                end={[1, 0.5]}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate("Home");
                    }}
                    style={{
                        flex: 1,
                        alignItems: "center"
                    }}
                >
                    <SvgUri
                        width={40}
                        height={40}
                        source={require("../../assets/MainMenu/home.svg")}
                    />
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: "Prompt",
                            color: "#ffffff",
                            textAlign: "center"
                        }}
                    >
                        Home
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate("ScanProduct");
                    }}
                    style={{
                        flex: 1,
                        alignItems: "center"
                    }}
                >
                    <SvgUri
                        width={40}
                        height={40}
                        source={require("../../assets/MainMenu/qrcode.svg")}
                    />
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: "Prompt",
                            color: "#ffffff",
                            textAlign: "center"
                        }}
                    >
                        Scan QR
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate("MyBasket");
                    }}
                    style={{
                        flex: 1,
                        alignItems: "center"
                    }}
                >
                    <View>
                        <SvgUri
                            width={40}
                            height={40}
                            source={require("../../assets/MainMenu/shopping_basket.svg")}
                        />
                        {this.props.basket.length > 0 && (
                            <View
                                style={{
                                    width: 22,
                                    height: 22,
                                    backgroundColor: "#ff1c1c",
                                    borderRadius: 22,
                                    position: "absolute",
                                    top: 0,
                                    right: -5
                                }}
                            >
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 14,
                                        letterSpacing: -0.23,
                                        textAlign: "center",
                                        color: "#ffffff"
                                    }}
                                >
                                    {this.props.basket.length}
                                </Text>
                            </View>
                        )}
                    </View>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: "Prompt",
                            color: "#ffffff",
                            textAlign: "center"
                        }}
                    >
                        My Basket
                    </Text>
                </TouchableOpacity>
            </LinearGradient>
        );
    }
}

// export default withNavigation(BottomBar);

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default withNavigation(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(BottomBar)
);
