import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";
import { withNavigation } from "react-navigation";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../action";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

class Promotion extends React.PureComponent {
    render() {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.navigation.navigate("Promotion", {
                        promo: this.props.item
                    });
                }}
            >
                <Image
                    style={{
                        width: SCREEN_WIDTH - 170,
                        height: SCREEN_WIDTH - 170
                    }}
                    source={this.props.item.png}
                />
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default withNavigation(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Promotion)
);
