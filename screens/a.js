import React, { Component, Fragment } from "react";


import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

import { TouchableHighlight,AppRegistry, FlatList, StyleSheet, Text, View , Image,
         StatusBar, Platform,Alert, TouchableOpacity} from 'react-native';

import SafeAreaView from "react-native-safe-area-view";
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

//data_item = [{item_code:'1001052'}]

class a extends Component {

  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
    this.state = { data: [],
      total: {
        discount: 0,
        amount: 0,
        grand_total: 0,
        total: 0,
        rate: 0
      }
    }
  }
  render() {
    var [discount, amount,grand_total,total] = [0,0,0,0];
    var listimage = [];
    var data2 = [];
    var getbasket = [];
    var rate = 40
    if(this.props.persona.churn) rate = 60;
    //this.props.persona.id
    let basketuri = 'http://1teamapi.azurewebsites.net/getbasket/'+this.props.persona.id;
    fetch(basketuri)
    .then(handleErrors)
    .then(response => response.json())
    .then(basket => {console.log(basket);
                     getbasket = basket;
      fetch('http://1teamapi.azurewebsites.net/getpicture/')
      .then(handleErrors)
      .then(response => response.json())
      .then(list => {//console.log(list)
                     listimage = list;
                     //console.log("getbasket"+getbasket[0]);
                     //console.log("listimage"+listimage[0]);

                     for (i = 0; i < getbasket.length; i++) { 
                      //console.log(getbasket[i].item_code)
                      for (j = 0; j < listimage.length; j++) { 
                        if(getbasket[i].item_code == listimage[j].item_code) {//console.log(listimage[j]);
                          amount++;
                          total = total+((getbasket[i].price * getbasket[i].number) - (getbasket[i].price * getbasket[i].promo) );
                          //console.log({"item_code":getbasket[i].item_code,"price":getbasket[i].price,"item_name":getbasket[i].item_name.substring(0, 10),"number":getbasket[i].number,"image":listimage[j].item_pic});
                            //console.log(JSON.stringify({"item_code":getbasket[i].item_code,"price":getbasket[i].price,"item_name":getbasket[i].item_name,"number":getbasket[i].number}))
                            data2.push({"item_code":getbasket[i].item_code,"price":getbasket[i].price,"item_name":getbasket[i].item_name.substring(0, 20)+"...","number":getbasket[i].number,"image":listimage[j].item_pic});
                            //console.log("data "+data2);
                            if(i+1 == getbasket.length) {
                              console.log("data "+data2);
                              discount = (total * rate / 100).toFixed(2);
                              this.setState({
                              data: data2,
                              total:{amount: amount,
                              total: total.toFixed(2),
                              discount: discount,
                              grand_total: (total-discount).toFixed(2),
                              rate: rate
                              }
                              });
                            }
                            break;
                        }
                        //if((j+1) == listimage.length) console.log("data "+data);
                        
  
                      }
                    }
                     
                       
                      }
      )
                    }
    )

    

    return (
      <Fragment>
        <SafeAreaView style={{ flex: 1, backgroundColor: '#053e8b' }}>
        <View style={{
                width: "100%",
                height: STATUS_BAR_HEIGHT,
                backgroundColor: "#053e8b"
            }}>
                <StatusBar
                    barStyle="light-content"
                />
        </View>
        {/* display */}
        <View style={styles.container_top}>
          {/* <TouchableOpacity
              onPress={() => {
                  this.props.navigation.goBack();
              }}
              style={{
                  position: "absolute",
                  marginTop: 20
              }}
          >
              <Text
                  style={{
                      fontSize: 40,
                      fontFamily: "Prompt",
                      color: "#0054c6",
                      marginLeft: 30
                  }}
              >
                  ‹
              </Text>
          </TouchableOpacity>     */}
          <Text style={styles.top_text}>
              Shopping Cart
          </Text>
        </View> 
        <View style={styles.container}>
          {/* <View style={styles.product_detail}>
            <Image style={styles.p_img}
              source={require('../assets/Design/2101572.png')}
            />
            <Text style={styles.p_detail} >
            ซีพี สปาเก็ตตี้ไก่สับ {"\n"}ขนาด 250 กรัม 3 ซอง{"\n"}฿30.18 
            </Text>
            <Text style={styles.p_detail2}>
            2 
            </Text>
          </View>
          */}
          <FlatList style={{height:250}}
          // data={[
          //   {"item_code":"1","userid":"17067","price":"111","productname":"ซีพี สปาเก็ตตี้ไก่สับ", 
          //    "p_detail":"ขนาด 250 กรัม 3 ซอง", "image":'../assets/Design/2101572.png'},
          //   {"item_code":"2","userid":"17067","price":"113","productname":"ซีพี สปาเก็ตตี้ไก่สับ", 
          //   "p_detail":"ขนาด 250 กรัม 3 ซอง", "image":'../assets/Design/2101572.png'},
          //   {"item_code":"3","userid":"17067","price":"11","productname":"ซีพี สปาเก็ตตี้ไก่สับ", 
          //   "p_detail":"ขนาด 250 กรัม 3 ซอง", "image":'../assets/Design/2101572.png'},
          //   {"item_code":"2","userid":"17067","price":"12","productname":"ซีพี สปาเก็ตตี้ไก่สับ", 
          //   "p_detail":"ขนาด 250 กรัม 3 ซอง", "image":'../assets/Design/2101572.png'},
          //   {"item_code":"2","userid":"17067","price":"12","productname":"ซีพี สปาเก็ตตี้ไก่สับ", 
          //   "p_detail":"ขนาด 250 กรัม 3 ซอง", "image":'../assets/Design/2101572.png'}
          // ]}
          data = {this.state.data}
          renderItem={({item}) => <View style={styles.product_detail}>
            <Image style={styles.p_img}
                  source={{uri: item.image}}
                   //source={require('../assets/Design/2101572.png')}
            />
            <View>
              <Text style={styles.p_detail}>{item.item_name}</Text>
              {/* <Text style={styles.p_detail}>{item.p_detail}</Text> */}
              <Text style={styles.p_detail}>{"฿" + item.price}</Text>
            </View>
            <Text style={styles.p_detail2}>{item.number}</Text>
          </View>}
        />

         
          <View style={styles.summary} >
            <Text style={styles.p_detail}>
            {this.state.total.amount} items in your cart {"\n"}Total {"\n"}<Text style={{color:'red'}}>Discount ({this.state.total.rate}%)</Text>{"\n"}<Text style={{color:'#0054c6'}}>Grand Total
            </Text></Text>
            <Text style={styles.p_detail}>
            {"\n"}฿ {this.state.total.total}{"\n"}<Text style={{color:'red'}}>฿ {this.state.total.discount}</Text>{"\n"}<Text style={{color:'#0054c6'}}>฿ {this.state.total.grand_total}</Text>
            </Text>
            <TouchableHighlight style={styles.button } onPress={this.onPress} underlayColor='#99d9f4'>
                <Text style={styles.buttonText}>Checkout</Text>
            </TouchableHighlight>   
          </View>
          <View style={styles.footers} >
                <View style={{flex: 1, marginLeft: 75}}  >
                <TouchableHighlight onPress={() => this.props.navigation.navigate("NameInput")}>
                    <Image style={styles.foot_icon} 
                    source={require('../assets/shopping.png')}
                    />
                    </TouchableHighlight>
                </View>
                <View style={{flex: 1, marginLeft: 15}}  >
                <TouchableHighlight onPress={() => this.props.navigation.navigate("ScanProduct")}>
                    <Image style={styles.foot_icon}
                    source={require('../assets/barcode.png')}
                    />
                </TouchableHighlight>
                </View>
                
                <View style={{flex: 1, marginLeft: 15}}  >
                <TouchableHighlight onPress={() => this.props.navigation.navigate("a")}>
                    <Image style={styles.foot_icon}
                    source={require('../assets/shopping-cart_active.png')}
                    />
                    </TouchableHighlight>
                </View>
            </View>  
              
        </View>
      </SafeAreaView>
    </Fragment>
    );
  }

  onPress() {
    Alert.alert(
      'S1 Retail Shop',
      'ขอบคุณที่ใช้บริการค่ะ',
      [
        { text: 'ตกลง', onPress: () => {
          this.props.addName({persona: {
            name: "",              
            churn: false,
            image: "",
            id: ""}});
          this.props.navigation.navigate("Welcome");} },
      ],
      { cancelable: false }
    );
    
    
    
  }
}

const styles = StyleSheet.create({
  container_top: {
    height:60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: 'grey',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 4,
  },
  top_text:{
    textAlign: 'center', fontSize:25, color: "#0054c6", fontFamily: "Prompt"
  }, 
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#f7f7fa',
  }, 
  product_detail:{
    backgroundColor: '#ffffff',
    borderRadius: 16,
    alignItems: 'flex-start',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    justifyContent: "space-between",
    marginTop:13,
    padding:15,
    paddingRight:50,
  },
  p_img:{
    height: 70, width: 70, resizeMode: "contain", marginLeft: 20,
  },
  p_detail:{
    textAlign: 'left', fontSize:15, color: "#000000", fontFamily: "Prompt", marginLeft: 15,
  },
  p_detail2:{
    textAlign: 'left', fontSize:25, color: "#0054c6", fontFamily: "Prompt", marginTop:15, 
     marginLeft: 30,
  },
  summary:{
    opacity: 0.8,
    backgroundColor: '#ffffff',
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    padding:10,
    marginTop:10,
  },
  p_sum:{
    textAlign: 'left', fontSize:15, color: "#000000", fontFamily: "Prompt", marginLeft: 15,
  },
  footers: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    justifyContent: "space-between",
    marginTop:10

  },
  foot_icon: {
    width:30,
    height: 30,
    resizeMode: "contain"   
  },
  button: {
    height: 54,
    width: 100,  
    backgroundColor: '#0054c6',
    borderColor: '#0054c6',
    marginTop: 10,
    borderRadius: 30,
    marginBottom: 10,
    //alignSelf: 'stretch',
    marginLeft: 30,
    justifyContent: 'center'
  },buttonText: {
      fontSize: 18,
      color: 'white',
      alignSelf: 'center'
  },
  item: {
    padding: 20,
    fontSize: 18,
    height: 44,
  },
  itemprice: {
    textAlign: 'right',
    padding: 20,
    fontSize: 18,
    height: 44,
  },

})

 




// skip this line if using Create React Native App
//AppRegistry.registerComponent('AwesomeProject', () => FlatListBasics);

function handleErrors(response) {
  if (!response.ok) {
      count = 0;
  }
  return response;
}

const mapStateToProps = state => {
    return {
     persona: state.reducer.persona
    };
};

const mapDispatchToProps = dispatch => {
   return bindActionCreators(Actions, dispatch);
};

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(a);