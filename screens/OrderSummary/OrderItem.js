import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

const SCREEN_WIDTH = Dimensions.get("window").width;

export default class OrderItem extends React.PureComponent {
    render() {
        return (
            <React.Fragment>
                <View
                    style={{
                        width: SCREEN_WIDTH - 40,
                        flexDirection: "row"
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: "#fff",
                            alignItems: "center",
                            borderRadius: 4
                        }}
                    >
                        <Image
                            style={{
                                width: (SCREEN_WIDTH - 40) / 3 - 20,
                                height:
                                    (((SCREEN_WIDTH - 40) / 3 - 20) * 352) /
                                    348,
                                marginHorizontal: 10,
                                marginVertical: 10
                            }}
                            source={this.props.item.pic}
                        />
                    </View>
                    <View
                        style={{
                            flex: 2,
                            justifyContent: "space-between"
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "space-between"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 20,
                                    color: "#ffffff",
                                    marginLeft: 20
                                }}
                            >
                                {this.props.item.name}
                            </Text>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "space-between",
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 12,
                                    color: "#f7ce68",
                                    textAlign: "justify",
                                    marginLeft: 20
                                }}
                            >
                                {this.props.item.remark}
                            </Text>

                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: "row",
                                    justifyContent: "flex-end",
                                    alignItems: "center"
                                }}
                            >
                                {this.props.item.fullPrice !=
                                    this.props.item.price && (
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 18,
                                            color: "#ffffff",
                                            textDecorationLine: "line-through",
                                            textDecorationStyle: "solid",
                                            marginRight: 15
                                        }}
                                    >
                                        {this.props.item.fullPrice *
                                            this.props.item.amount}
                                    </Text>
                                )}
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        color: "#ffffff",
                                        marginRight: 15
                                    }}
                                >
                                    {this.props.item.price *
                                        this.props.item.amount}
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                justifyContent: "flex-end",
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 16,
                                    color: "#ffffff",
                                    marginRight: 15
                                }}
                            >
                                x{this.props.item.amount}
                            </Text>
                        </View>
                    </View>
                </View>
                <View
                    style={{
                        width: SCREEN_WIDTH - 40,
                        height: 2,
                        backgroundColor: "#979797",
                        marginVertical: 10
                    }}
                />
            </React.Fragment>
        );
    }
}
