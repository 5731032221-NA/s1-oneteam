export const UPDATE_PERSONA = "UPDATE_PERSONA";
export const UPDATE_SELECTED_CARD_INDEX = "UPDATE_SELECTED_CARD_INDEX";
export const ADD_BASKET = "ADD_BASKET";
export const REMOVE_BASKET = "REMOVE_BASKET";
export const CLEAR_BASKET = "CLEAR_BASKET";
export const UPDATE_PERSONAS = "UPDATE_PERSONAS";
export const UPDATE_ITEMS = "UPDATE_ITEMS";
export const INCREASE_BASKET = "INCREASE_BASKET";
export const DECREASE_BASKET = "DECREASE_BASKET";
export const ADD_NAME = "ADD_NAME";

export const addName = payload => {
    return {
        type: ADD_NAME,
        payload: payload
    };
};

export const updatePersona = payload => {
    return {
        type: UPDATE_PERSONA,
        payload: payload
    };
};

export const updateSelectedCardIndex = payload => {
    return {
        type: UPDATE_SELECTED_CARD_INDEX,
        payload: payload
    };
};

export const addBasket = payload => {
    return {
        type: ADD_BASKET,
        payload: payload
    };
};

export const removeBasket = payload => {
    return {
        type: REMOVE_BASKET,
        payload: payload
    };
};

export const clearBasket = payload => {
    return {
        type: CLEAR_BASKET,
        payload: payload
    };
};

export const updatePersonas = payload => {
    return {
        type: UPDATE_PERSONAS,
        payload: payload
    };
};

export const updateItems = payload => {
    return {
        type: UPDATE_ITEMS,
        payload: payload
    };
};

export const increaseBasket = payload => {
    return {
        type: INCREASE_BASKET,
        payload: payload
    };
};

export const decreaseBasket = payload => {
    return {
        type: DECREASE_BASKET,
        payload: payload
    };
};
