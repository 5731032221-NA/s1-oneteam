import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

export default class Tutorial extends React.PureComponent {
    render() {
        return (
            <View
                style={{
                    alignItems: "center"
                }}
            >
                <View style={{ height: 10 }} />
                <Image
                    style={{
                        width: SCREEN_HEIGHT - 450,
                        height: SCREEN_HEIGHT - 450
                    }}
                    width={SCREEN_HEIGHT - 450}
                    height={SCREEN_HEIGHT - 450}
                    source={this.props.item.png}
                />
                <Text
                    style={{
                        fontSize: 22,
                        color: "#c3e1fa",
                        fontFamily: "Prompt_Bold",
                        marginTop: 16,
                        textShadowColor: "#00000088",
                        textShadowOffset: {
                            width: 0,
                            height: 2
                        },
                        textShadowRadius: 4,
                        letterSpacing: 0.5
                    }}
                >
                    {this.props.item.title}
                </Text>
                <Text
                    style={{
                        fontSize: 16,
                        color: "#ffffff",
                        fontFamily: "Prompt",
                        textAlign: "center",
                        marginTop: 16
                    }}
                >
                    {this.props.item.detail}
                </Text>
            </View>
        );
    }
}
