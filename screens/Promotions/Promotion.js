import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

export default class Promotion extends React.PureComponent {
    render() {
        return (
            <Image
                style={{
                    width: (SCREEN_HEIGHT - 170) / 2 - 60,
                    height: (SCREEN_HEIGHT - 170) / 2 - 60,
                    marginHorizontal: 5
                }}
                source={this.props.item.png}
            />
        );
    }
}
