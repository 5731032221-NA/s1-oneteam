import { createStackNavigator, createAppContainer } from "react-navigation";
import { fromRight } from "react-navigation-transitions";

import NameInput from "./NameInput";
import TestRedux from "./TestRedux";
import CameraProf from "./CameraProf";
import Welcome from "./Welcome";
// import ScanPersona from "./ScanPersona";
import Home from "./Home";
import ScanProduct from "./ScanProduct";
// import AddToBasket from "./AddToBasket";
// import MyBasket from "./MyBasket";
// import MyCard from "./MyCard";
// import OrderSummary from "./OrderSummary";
// import ThankYou from "./ThankYou";
// import Promotion from "./Promotion";
//import Goog from "./Goog";
import a from "./a";

const AppNavigator = createStackNavigator(
    {
        //Goog: Goog,
        a: a,
        Welcome: Welcome,
        NameInput: NameInput,
        CameraProf: CameraProf,
        ScanProduct: ScanProduct,
        // ScanPersona: ScanPersona,
        Home: Home,
        TestRedux: TestRedux,
        // AddToBasket: AddToBasket,
        // MyBasket: MyBasket,
        // MyCard: MyCard,
        //OrderSummary: OrderSummary,
        // ThankYou: ThankYou,
        // Promotion: Promotion
    },
    {
        initialRouteName: "Welcome",
        headerMode: "none"
        // transitionConfig: () => fromRight()
    } 
);

export default createAppContainer(AppNavigator);
