import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import * as Permissions from 'expo-permissions';
import * as Constants from 'expo-constants'
import { Camera } from 'expo-camera';
import * as MediaLibrary from 'expo-media-library'
import ReactLoading from 'react-loading';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";
import Spinner from 'react-native-loading-spinner-overlay';



class CameraProf extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    loading: false,
    rollGranted: true,
    cameraGranted: true,
    hasCameraPermission: null,
    scanned: false
  };

  async componentWillMount(){
    this.getCameraPermissions();
  }

  async getCameraPermissions() {
    //const { Permissions } = Expo;
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    // if (status === 'granted') {
    //   this.setState({ cameraGranted: true });
    // } else {
    //   this.setState({ cameraGranted: false });
    //   console.log('Uh oh! The user has not granted us permission.');
    // }
    this.setState({ cameraGranted: status === "granted" });
    this.setState({type: Camera.Constants.Type.front});
    this.getCameraRollPermissions();
    //const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    
    //this.setState({ rollGranted: status === "granted" });
  }

  async getCameraRollPermissions() {
    // const { Permissions } = Expo;
    // const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    // if (status === 'granted') {
    //   this.setState({ rollGranted: true });
    // } else {
    //   console.log('Uh oh! The user has not granted us permission.');
    //   this.setState({ rollGranted: false });
    // }
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    
    this.setState({ rollGranted: status === "granted" });
  }


  
    async uploadLocalFile(aborter, containerURL, filePath) {



      filePath = path.resolve(filePath);



      const fileName = path.basename(filePath);

      const blockBlobURL = BlockBlobURL.fromContainerURL(containerURL, fileName);



      return await uploadFileToBlockBlob(aborter, filePath, blockBlobURL);

    }

  takePictureAndCreateAlbum = async () => {
    this.setState({
      //change the state of the laoding in every 3 second
      loading: true,
    });
    let data = {};
    data.name = this.props.persona.name;
    data.churn = this.props.persona.churn;

    console.log('tpaca');
    const { uri } = await this.camera.takePictureAsync();
    console.log('uri', uri);
    
    const asset = await MediaLibrary.createAssetAsync(uri);
    console.log('asset', asset);
    MediaLibrary.createAlbumAsync('Expo', asset)
      .then(() => {
        //<ReactLoading type={type} color={color} height={'20%'} width={'20%'} />
      })
      .catch(error => {
        Alert.alert('An Error Occurred!')
      });
    //this.props.navigation.navigate("Home", { fromScan: true });
    uploadResponse = await uploadImageAsync(uri);
    // uploadResult = await uploadResponse.json();
    //this.props.persona.name
    console.log('uploadResponse',uploadResponse);
    uploadResponsejson = await uploadResponse.json();
    console.log('uploadResponse2',uploadResponsejson);
    console.log('uploadResponse3',JSON.parse(uploadResponsejson));
    // console.log(uploadResponse.json());
    // console.log(uploadResponse.json());
    data.image = "https://oneteamblob.blob.core.windows.net/profilepicture/"+JSON.parse(uploadResponsejson).Uploaded;
    console.log('data');
    console.log(data);
    // v = await fetch(
    //   `${Expo.Constants.manifest.extra.fxUrl}register?code=${
    //       Expo.Constants.manifest.extra.fxKey
    //   }`,
    //   { method: "POST", body: JSON.stringify(data) }
    // )
    v = '';
    console.log("buggg")
    console.log(v);
    console.log("bugg")
    //console.log(v._bodyText);
    console.log("bug")
    console.log(v._bodyText);
    this.props.addName({
      name: this.props.persona.name,
      churn: this.props.persona.churn,
      // , v._bodyText
      //id: v._bodyText use!!!!!!!
      id:70
    });


    
    console.log('uploadResult');
    this.setState({
      //change the state of the laoding in every 3 second
      loading: false
    });
    // console.log(uploadResult);
    // console.log(uploadResult.location);
    this.props.navigation.navigate("ScanProduct");
    //this.props.navigation.navigate("TestRedux");
  
  };

  onPress() {
    
    this.props.navigation.navigate("Nameinput");
    
  }
  

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          //visibility of Overlay Loading Spinner
          visible={this.state.loading}
          //Text with the Spinner
          textContent={'Loading...'}
          //Text style of the Spinner Text
          textStyle={styles.spinnerTextStyle}
        />
        <Camera
          type={Camera.Constants.Type.front}
          style={{ flex: 1 }}
          ref={ref => {
            this.camera = ref;
          }}
        />
        <TouchableOpacity
          onPress={() =>
            this.state.rollGranted && this.state.cameraGranted
              ? this.takePictureAndCreateAlbum()
              : Alert.alert('Permissions not granted')
            
          }
          

          style={styles.buttonContainer}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Take a profile picture
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

async function uploadImageAsync(uri) {
  let apiUrl = 'https://s1-demo.herokuapp.com/upload';

  // Note:
  // Uncomment this if you want to experiment with local server
  //
  // if (Constants.isDevice) {
  //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
  // } else {
  //   apiUrl = `http://localhost:3000/upload`
  // }

  let uriParts = uri.split('.');
  let fileType = uriParts[uriParts.length - 1];

  let formData = new FormData();
  formData.append('photo', {
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });

  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };

  return await  fetch(apiUrl, options);
}



const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 30,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  button: {
    width: 200,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    paddingVertical: 4,
    borderWidth: 1.5,
    borderColor: '#fff',
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
  },
});

const mapStateToProps = state => {
  return {
      persona: state.reducer.persona
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraProf);