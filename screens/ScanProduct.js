import React, { Component, Fragment  } from 'react';

import {
    Image,
    TouchableHighlight ,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Alert,
    StatusBar, Platform, TouchableOpacity
} from "react-native";
// import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input, Button } from "react-native-elements";
//import {  BarCodeScanner, Permissions, Camera } from "expo";
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { LinearGradient } from 'expo-linear-gradient'
import SafeAreaView from "react-native-safe-area-view";
import { withNavigation } from "react-navigation";
import { ProgressDialog,Dialog } from "react-native-simple-dialogs";


import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

//let ITEMS = [];
var count = 0;
class ScanProduct extends React.Component {
    
    constructor(props) {
        super(props);
        //this.done = this.done.bind(this);
        this.state = {
            // ddata: null,
            hasCameraPermission: null,
            pic: 'https://upload.wikimedia.org/wikipedia/en/thumb/9/98/Blank_button.svg/1124px-Blank_button.svg.png',
            item_id: '',
            price: 0,
            item_name: '',
            count: 0,
            promo: '',
            showDialog: false
        };

    }
    openDialog = (show) => {
        this.setState({ showDialog: show });
    }
    openProgress = (show) => {
        this.setState({ showProgress: show });

        // setTimeout(
        //     () => {
        //         this.setState({ showProgress: false });
        //     },
        //     4000,
        // );
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === "granted" }
        );
        // this.setState({type: Camera.Constants.Type.front})

        // ITEMS = await this.props.items.reduce((total, value) => {
        //     return total.concat(value.item_code);
        // }, []);
    }

    render() {

        
        const { hasCameraPermission } = this.state;

        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <Fragment>
                    <SafeAreaView style={{ flex: 1, backgroundColor: '#053e8b' }}>
                    <View style={{
                            width: "100%",
                            height: STATUS_BAR_HEIGHT,
                            backgroundColor: "#053e8b"
                        }}>
                            <StatusBar
                                barStyle="light-content"
                            />
                    </View>
                    {/* display */}
                    <View style={styles.container_top}>
                    {/* <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                        style={{
                            position: "absolute",
                            marginTop: 20
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 40,
                                fontFamily: "Prompt",
                                color: "#0054c6",
                                marginLeft: 30
                            }}
                        >
                            ‹
                        </Text>
                    </TouchableOpacity>     */}
                    <Text style={styles.top_text}>
                        Scan Product
                    </Text>
                    </View> 
                    <View
                        style={{
                            backgroundColor: "#f7f7fa",
                            alignItems: "center",
                            justifyContent: "center",
                            flex: 1
                        }}
                    >
                        
                        <View
                            style={{
                                //position: "absolute",
                                width: SCREEN_WIDTH ,
                                height: SCREEN_WIDTH + 120 ,
                                //top: 10,
                                //bottom: 30,
                                borderWidth: 1,
                                borderColor: "#24bce0"
                            }}
                        >
                            <Camera
                                onBarCodeScanned={this._handleBarCodeRead}
                                style={{ flex: 1 }}
                                barCodeScannerSettings={{
                                    barCodeTypes: [
                                        BarCodeScanner.Constants.BarCodeType.qr
                                    ]
                                }}
                            />
                            <View
                            style={{
                                width: SCREEN_WIDTH,
                                position: "absolute",
                                top: 10,
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    //fontFamily: "Prompt",
                                    fontSize: 20,
                                    letterSpacing: 0.1,
                                    textAlign: "center",
                                    color: "white",
                                    fontWeight: "bold",
                                    borderColor: "black",
                                    borderRadius: 1
                                }}
                            >
                                ยินดีต้อนรับคุณ {"\n"}
                                { this.props.persona.name }
                            </Text>
                            <Text
                                style={{
                                   
                                    fontFamily: "Prompt",
                                    fontSize: 16,
                                    letterSpacing: 0.1,
                                    textAlign: "center",
                                    color: "white",
                                    borderColor: "black",
                                    borderRadius: 1
                                    //color: "#575757"
                                }}
                            >
                                เริ่ม ScanQR สินค้า{"\n"}
                                ที่คุณถูกใจได้เลย!
                            </Text>
                        </View>
                        </View>

                        <Dialog
                    title="เพิ่มสินค้า"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={ () => {this.openDialog(false);
                        this.setState({
                            count:0
                            });} }
                    visible={ this.state.showDialog }
                >
                    <Image
                        source={
                            {
                                uri: this.state.pic,
                            }
                        }
                        style={
                            {
                                width: 99,
                                height: 87,
                                //backgroundColor: "white",
                                marginTop: 10,
                                resizeMode: "contain",
                            }
                        }
                    />
                    <Text style={ { marginVertical: 30 ,textAlign: 'center', fontFamily: "Prompt",} }>
                        {this.state.item_name}{"\n"}
                        <Text style={ { marginTop:20, textAlign: 'center', fontFamily: "Prompt",
                                        color: "#0054c6", fontSize:17, fontWeight:"bold" } }>
                            ราคา {this.state.price}{" บาท"}
                       </Text>    
                    </Text>
                    <View style={{
                            marginTop:-20,
                            flexDirection: 'row',
                            justifyContent: "center", 
                            width:160,
                            height:40,
                             }}>
                    <Button
                        onPress={ () => {
                            if(this.state.promo == 'y'){
                                fetch('http://1teamapi.azurewebsites.net/insertbasketoffer/', {
                                    method: 'post',
                                    headers: {'Content-Type':'application/json'},
                                    body: JSON.stringify({"item_code":this.state.item_id,"userid":this.props.persona.id,"price":this.state.price,"productname":this.state.item_name})
                                    });
                                }
                                else{
                                fetch('http://1teamapi.azurewebsites.net/insertbasket/', {
                                method: 'post',
                                headers: {'Content-Type':'application/json'},
                                body: JSON.stringify({"item_code":this.state.item_id,"userid":this.props.persona.id,"price":this.state.price,"productname":this.state.item_name})
                                });
                                }
                                
                                Alert.alert(
                                    'Notification',
                                    'สินค้าจำนวน 1 รายการได้ถูกเพิ่มลงในตะกร้าของท่าน',
                                    [
                                      { text: 'ตกลง', onPress: () =>  {this.setState({
                                        count:0
                                        });
                                        this.openDialog(false);
                                        this.props.navigation.navigate("a");
                                        } }

                                    ],
                                    { cancelable: false }
                                  );
                            }
                               
                             }
                        title=" เพิ่ม     "
                    /> 
                    <Text>{"\t\t"}</Text>
                    <Button
                        onPress={ () => {
                                this.setState({
                                    count:0
                                    });
                                this.openDialog(false)} }
                        style={ { marginTop:0 } }//,borderRadius: 30,borderColor:'#0054c6'
                        title=" ยกเลิก  "
                        type="outline"
                    />
                    </View>
                </Dialog>

                <ProgressDialog
                    title="Loading Please Wait"
                    activityIndicatorColor="blue"
                    activityIndicatorSize="large" 
                    animationType="slide"
                    message="กำลังอ่าน QRcode..."
                    visible={ this.state.showProgress }
                />
                        
                        {/* <TouchableHighlight style={styles.button } onPress={this.done} underlayColor='#99d9f4'>
                            <Text style={styles.buttonText}>Done</Text>
                        </TouchableHighlight>    */}


                        <View style={styles.footers} >
                            <View style={{flex: 1, marginLeft: 75}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("NameInput")}>
                                <Image style={styles.foot_icon} 
                                source={require('../assets/shopping.png')}
                                />
                                </TouchableHighlight>
                            </View>
                            <View style={{flex: 1, marginLeft: 15}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("ScanProduct")}>
                                <Image style={styles.foot_icon}
                                source={require('../assets/barcode_active.png')}
                                />
                            </TouchableHighlight>
                            </View>
                            
                            <View style={{flex: 1, marginLeft: 15}}  >
                            <TouchableHighlight onPress={() => this.props.navigation.navigate("a")}>
                                <Image style={styles.foot_icon}
                                source={require('../assets/shopping-cart.png')}
                                />
                                </TouchableHighlight>
                            </View>
                        </View>


                    </View>

                   </SafeAreaView> 
                </Fragment>
            );
        }
        
    }

    _handleBarCodeRead = ({ type, data }) => {
        console.log(data);
        console.log("count:"+this.state.count);
        //let str = {"item_code":"11149","userid":"17067","price":"111","productname":"d"};
        // if(!this.state.showDialog) {
        //     this.setState({
        //         count:0
        //         });
        // }
        if(this.state.count == 0){
            this.openProgress(true)
            this.setState({
                count:1
                });
            // try{
            fetch(data)
                .then(handleErrors)
                .then(response => response.json())
                //then(response => response.json)
                // .then(ddata => this.setState({ ddata }))
                // .then(console.log(this.state.ddata));
                //.then(ddata => console.log(ddata))
                .then(ddata =>  {
                    fetch('http://1teamapi.azurewebsites.net/getpicture/')
                    .then(handleErrors)
                    .then(response => response.json())
                    .then(list => {
                                    console.log(ddata[0].item_name);
                                    for (i = 0; i < list.length; i++) { 
                                        if(list[i].item_code == ddata[0].item_id ) {this.setState({
                                            pic: list[i].item_pic,
                                            item_id: ddata[0].item_id,
                                            price: ddata[0].price,
                                            item_name: ddata[0].item_name,
                                            promo: ddata[0].promo
                                            });
                                            this.openProgress(false);
                                            this.openDialog(true);
                                            break;
                                        }
                                    }
                    })
                    

                //     Alert.alert(
                        

                //         <Image style={styles.p_img}
                //         source={{uri: this.state.pic}}
                //   />+'เพิ่มสินค้า',
                //     ddata[0].item_name,
                //     [
                //     {text: 'เพิ่ม', onPress: () => {console.log(ddata);
                //                                 console.log({"item_code":ddata[0].item_id,"userid":this.props.persona.id,"price":ddata[0].price,"productname":ddata[0].item_name});
                //                                   count = 0;
                //                                   if(ddata[0].promo == 'y'){
                //                                     fetch('http://1teamapi.azurewebsites.net/insertbasketoffer/', {
                //                                         method: 'post',
                //                                         headers: {'Content-Type':'application/json'},
                //                                         body: JSON.stringify({"item_code":ddata[0].item_id,"userid":this.props.persona.id,"price":ddata[0].price,"productname":ddata[0].item_name})
                //                                        });
                //                                   }
                //                                   else{
                //                                     fetch('http://1teamapi.azurewebsites.net/insertbasket/', {
                //                                     method: 'post',
                //                                     headers: {'Content-Type':'application/json'},
                //                                     body: JSON.stringify({"item_code":ddata[0].item_id,"userid":this.props.persona.id,"price":ddata[0].price,"productname":ddata[0].item_name})
                //                                    });
                //                                   }
                //                                 }
                                                
                //                                 },
                //     {
                //         text: 'ยกเลิก',
                //         onPress: () => {console.log('Cancel Pressed');
                //                         count = 0},
                //         style: 'cancel',
                //     }
                //     //   ,
                //     //   {text: 'OK', onPress: () => console.log('OK Pressed')},
                //     ],
                //     {cancelable: false},
                // )
                }
                );
        // }catch(err)
        // {
        //     count=0;
        // }
        }
        // if (ITEMS.includes(data)) {
        //     let item = this.props.items.reduce((total, value) => {
        //         if (value.item_code == data) {
        //             total = value;
        //         }
        //         return total;
        //     }, []);

        //     // let promos = this.props.persona.promos.reduce((total, value) => {
        //     //     return total.concat(value.itemCode);
        //     // }, []);

        //     let allPromos = this.props.persona.allPromos.reduce(
        //         (total, value) => {
        //             return total.concat(value.itemCode);
        //         },
        //         []
        //     );

        //     let matchPromo = this.props.persona.allPromos.reduce(
        //         (total, value) => {
        //             if (value.itemCode == data) {
        //                 total = value;
        //             }
        //             return total;
        //         },
        //         []
        //     );

        //     let scannedItem = {
        //         code: item.item_code,
        //         pic: { uri: item.item_pic },
        //         name: item.item_name,
        //         detail: item.item_desc,
        //         remark: allPromos.includes(data) ? "S1คัดสรรเพื่อคุณ" : "",
        //         fullPrice: item.sales_price,
        //         price: allPromos.includes(data)
        //             ? matchPromo.promoPrice
        //             : item.sales_price,
        //         amount: 1,
        //         proId: allPromos.includes(data) ? matchPromo.promoId : ""
        //     };

        //     this.props.navigation.replace("AddToBasket", {
        //         scannedItem: scannedItem
        //     });
        // }
    };

    // done() {
        
    //       this.props.navigation.navigate("a");
        
    // }
}

const opacity = "rgba(0, 0, 0, .6)";
const styles = StyleSheet.create({
    p_img:{
        height: 70, width: 70, resizeMode: "contain", marginLeft: 20,
      },
    container_top: {
        height:60,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: 'grey',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        elevation: 4,
    },
    top_text:{
    textAlign: 'center', fontSize:25, color: "#0054c6", fontFamily: "Prompt"
    },  
    container: {
        flex: 1,
        flexDirection: "column"
    },button: {
        height: 54,
        width: 280,
        backgroundColor: '#0054c6',
        borderColor: '#0054c6',
        //marginBottom: 48,
        borderRadius: 30,
        //marginBottom: 10,
        //alignSelf: 'stretch',
        justifyContent: 'center'
    },buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    layerTop: {
        flex: 1,
        backgroundColor: opacity
    },buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    layerCenter: {
        flex: 1,
        flexDirection: "row"
    },
    layerLeft: {
        flex: 1,
        backgroundColor: opacity
    },
    focused: {
        flex: 8,
        borderWidth: 1,
        borderColor: "#00b900"
    },
    layerRight: {
        flex: 1,
        backgroundColor: opacity
    },
    layerBottom: {
        flex: 1,
        backgroundColor: opacity
    },
    footers: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        justifyContent: "space-between",
       // marginBottom:30
    },
    foot_icon: {
        width:30,
        height: 30,
        resizeMode: "contain"  
    }
});

function handleErrors(response) {
    if (!response.ok) {
        count = 0;
    }
    return response;
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
        // ,
        // personas: state.reducer.personas,
        // items: state.reducer.items
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScanProduct);
