import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView,
    ActivityIndicator
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
//import { LinearGradient } from "expo";
import { LinearGradient } from 'expo-linear-gradient'
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import Tutorial from "./Welcome/Tutorial";
import LoadingOverlay from "./Shared/LoadingOverlay";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

const TUTORIALS = [
    {
        svg: require("../assets/Welcome/svg/welcome_ScanPersona.svg"),
        png: require("../assets/Welcome/png/welcome_ScanPersona.png"),
        title: "Choose your lifestyle",
        detail: `ScanQR เพื่อ Shop สินค้า
ที่ Lifestyle ตรงกับคุณที่สุด`
    },
    {
        svg: require("../assets/Welcome/svg/welcome_Promotion.svg"),
        png: require("../assets/Welcome/png/welcome_Promotion.png"),
        title: "Check out promotions",
        detail: `พบกับโปรโมชั่นต่างๆ
ที่จะส่งถึงมือคุณ`
    },
    {
        svg: require("../assets/Welcome/svg/welcome_Explore.svg"),
        png: require("../assets/Welcome/png/welcome_Explore.png"),
        title: "Explore many products",
        detail: `ง่ายและสนุกกับการช้อป
เพียงคุณ ScanQR สินค้าที่ต้องการ`
    }
];

console.disableYellowBox = true;

class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0,
            ready: false
        };
    }

    componentDidMount() {
       // this._fetchPersonas();
       this.setState({ ready: true });
    }

    _fetchPersonas = async () => {
        await fetch(
            `${Expo.Constants.manifest.extra.fxUrl}Personas?code=${
                Expo.Constants.manifest.extra.fxKey
            }`,
            {}
        )
            .then(res => res.json())
            .then(res => {
                this.props.updatePersonas(res);
                this._fetchItems();
            })
            .catch(error => {
                console.log(error.message);
                Alert.alert(
                    "Connection Problem",
                    "Please connect internet and press retry",
                    [
                        {
                            text: "Retry",
                            onPress: () => {
                                this._fetchPersonas();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            });
    };

    _fetchItems = async () => {
        await fetch(
            `${Expo.Constants.manifest.extra.fxUrl}Items?code=${
                Expo.Constants.manifest.extra.fxKey
            }`,
            {}
        )
            .then(res => res.json())
            .then(res => {
                this.props.updateItems(res);
                this.setState({ ready: true });
            })
            .catch(error => {
                console.log(error.message);
                Alert.alert(
                    "Connection Problem",
                    "Please connect internet and press retry",
                    [
                        {
                            text: "Retry",
                            onPress: () => {
                                this._fetchItems();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            });
    };

    onPressStart = () => {
        this.props.navigation.navigate("NameInput");
    };

    _renderTutorial = ({ item, index }) => {
        return <Tutorial item={item} />;
    };

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1,
                    alignItems: "center"
                }}
                colors={["#000000", "#1c1b2f", "#053e8b", "#053e8b", "#053e8b", "#1c1b2f", "#000000"]}
            >
                <View
                    style={{
                        alignItems: "center"
                    }}
                >
                    <Text
                        style={{
                            fontSize: 28,
                            color: "#ffffff",
                            marginTop: 50,
                            fontFamily: "Prompt"
                        }}
                    >
                        Welcome to S1
                    </Text>
                    <Text
                        style={{
                            fontSize: 18,
                            color: "#ffffff",
                            fontFamily: "Prompt"
                        }}
                    >
                        ช้อปง่าย ช้อปสนุก เพียงไม่กี่ขั้นตอน
                    </Text>
                    <View
                        style={{
                            height: SCREEN_WIDTH + 100
                        }}
                    >
                        <Carousel
                            data={TUTORIALS}
                            renderItem={this._renderTutorial}
                            sliderWidth={SCREEN_WIDTH}
                            itemWidth={SCREEN_WIDTH}
                            onSnapToItem={index => {
                                this.setState({ activeSlide: index });
                            }}
                        />
                    </View>
                </View>

                <View
                    style={{
                        position: "absolute",
                        bottom: 0,
                        width: SCREEN_WIDTH,
                        justifyContent: "flex-end"
                    }}
                >
                    {this.state.activeSlide < TUTORIALS.length - 1 && (
                        <Pagination
                            dotsLength={TUTORIALS.length}
                            activeDotIndex={this.state.activeSlide}
                            containerStyle={{}}
                            dotStyle={{
                                width: 10,
                                height: 10,
                                borderRadius: 5,
                                backgroundColor: "rgba(255, 255, 255, 0.92)"
                            }}
                            inactiveDotStyle={
                                {
                                    // Define styles for inactive dots here
                                }
                            }
                            inactiveDotOpacity={0.4}
                            inactiveDotScale={0.6}
                        />
                    )}
                    {this.state.activeSlide < TUTORIALS.length - 1 && (
                        <TouchableOpacity
                            onPress={this.onPressStart}
                            style={{
                                height: 40,
                                alignSelf: "center"
                            }}
                        >
                            <Text
                                style={{
                                    alignSelf: "center",
                                    fontSize: 12,
                                    fontFamily: "Prompt",
                                    color: "#ffffff"
                                }}
                            >
                                SKIP
                            </Text>
                        </TouchableOpacity>
                    )}
                    {this.state.activeSlide == TUTORIALS.length - 1 && (
                        <TouchableOpacity
                            onPress={this.onPressStart}
                            style={{
                                height: 40,
                                alignSelf: "center"
                            }}
                        >
                            {/* colors={["#c19963", "#f7ce68"]} */ }
                            <LinearGradient
                                
                                colors={["#0037ff", "#387bd9" ]}
                                start={[1, 0.5]}
                                end={[0, 0.5]}
                                style={{
                                    width: 189,
                                    height: 38,
                                    borderRadius: 100,
                                    alignSelf: "center"
                                }}
                            >
                                <Text
                                    style={{
                                        alignSelf: "center",
                                        fontSize: 18,
                                        fontFamily: "Prompt",
                                        marginTop: 4,
                                        color:'#ffffff'
                                    }}  
                                >
                                    Let’s Start
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    )}
                    <View style={{ height: 10 }} />
                    <Text
                        style={{
                            alignSelf: "center",
                            fontSize: 12,
                            fontFamily: "Prompt",
                            color: "grey"
                        }}
                    >
                        v.04.23.00
                    </Text>
                    <View style={{ height: 10 }} />
                </View>

                {!this.state.ready && <LoadingOverlay />}
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        personas: state.reducer.personas
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Welcome);
