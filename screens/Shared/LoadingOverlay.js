import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView,
    ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
//import { LinearGradient } from "expo";
import { LinearGradient } from 'expo-linear-gradient'
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { withNavigation } from "react-navigation";

export default class LoadingOverlay extends React.PureComponent {
    render() {
        return (
            <View
                style={[
                    StyleSheet.absoluteFillObject,
                    {
                        backgroundColor: "rgba(2, 2, 2, 0.8)",
                        zIndex: 5,
                        alignItems: "center",
                        justifyContent: "center"
                    }
                ]}
            >
                <ActivityIndicator size="large" color="#f7ce68" />
            </View>
        );
    }
}
