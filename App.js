import React from "react";
import { StyleSheet, Text, View, BackHandler } from "react-native";
//import { Font } from "expo";
import * as Font from 'expo-font'

import { Provider } from "react-redux";
import configureStore from "./store";
import AppNavigator from "./screens/AppNavigator";

const store = configureStore();

export default class App extends React.Component {
    state = {
        fontLoaded: false
    };

    async componentDidMount() {
        await Font.loadAsync({
            Prompt: require("./assets/fonts/Kanit-Regular.ttf"),
            Prompt_Bold: require("./assets/fonts/Kanit-Bold.ttf"),
            Prompt_LightItalic: require("./assets/fonts/Kanit-LightItalic.ttf"),
            Prompt_ExtraLight: require("./assets/fonts/Kanit-ExtraLight.ttf")
        });

        this.setState({ fontLoaded: true });
        BackHandler.addEventListener("hardwareBackPress", function() {
            return true;
        });
    }

    render() {
        return (
            <Provider store={store}>
                {this.state.fontLoaded ? <AppNavigator /> : null}
            </Provider>
        );
    }
}
