import {
    UPDATE_PERSONA,
    UPDATE_SELECTED_CARD_INDEX,
    ADD_BASKET,
    REMOVE_BASKET,
    CLEAR_BASKET,
    UPDATE_PERSONAS,
    UPDATE_ITEMS,
    INCREASE_BASKET,
    DECREASE_BASKET,
    ADD_NAME
} from "./action";

const initialState = {
    persona: {
        name: "",              
        churn: false,
        image: "",
        id: "",
        describe: "",
        subDescribe: "",
        cards: [],
        promos: [],
        specialPromos: []
    },
    basket: [],
    selectedCardIndex: 1,
    personas: [],
    items: [],
    Person: {
        name: "",              
        Churn: false,
        image: ""
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NAME:
            return {
                ...state,
                persona: action.payload
            };
        case UPDATE_PERSONA:
            return {
                ...state,
                persona: action.payload
            };
        case UPDATE_SELECTED_CARD_INDEX:
            return {
                ...state,
                selectedCardIndex: action.payload
            };
        case ADD_BASKET:
            var check = state.basket.filter(function(obj) {
                return obj.code !== action.payload.code;
            });
            var array = state.basket;
            if (array.length == check.length) {
                array = state.basket.concat(action.payload);
            }
            return {
                ...state,
                basket: array
            };
        case REMOVE_BASKET:
            var array = state.basket.filter(function(obj) {
                return obj.code !== action.payload;
            });
            return {
                ...state,
                basket: array
            };
        case CLEAR_BASKET:
            return {
                ...state,
                basket: []
            };
        case UPDATE_PERSONAS:
            return {
                ...state,
                personas: action.payload
            };
        case UPDATE_ITEMS:
            return {
                ...state,
                items: action.payload
            };
        case INCREASE_BASKET:
            var array = state.basket.reduce((total, value) => {
                if (value.code != action.payload) {
                    return total.concat(value);
                } else {
                    return total.concat({ ...value, amount: value.amount + 1 });
                }
            }, []);
            return {
                ...state,
                basket: array
            };
        case DECREASE_BASKET:
            var array = state.basket.reduce((total, value) => {
                if (value.code != action.payload || value.amount <= 1) {
                    return total.concat(value);
                } else {
                    return total.concat({
                        ...value,
                        amount: value.amount - 1
                    });
                }
            }, []);
            return {
                ...state,
                basket: array
            };
        default:
            return state;
    }
};

export default reducer;
